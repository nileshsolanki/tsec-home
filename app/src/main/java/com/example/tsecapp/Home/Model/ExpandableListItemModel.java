package com.example.tsecapp.Home.Model;

import java.util.List;

public class ExpandableListItemModel {

    String parentTitle;
    int icon;
    List<String> childItems;

    public ExpandableListItemModel(String parentTitle, int icon, List<String> childItems) {
        this.parentTitle = parentTitle;
        this.icon = icon;
        this.childItems = childItems;
    }


    public String getParentTitle() {
        return parentTitle;
    }

    public int getIcon() {
        return icon;
    }

    public List<String> getChildItems() {
        return childItems;
    }
}
