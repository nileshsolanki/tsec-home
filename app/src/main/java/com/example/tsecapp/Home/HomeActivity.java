package com.example.tsecapp.Home;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;


import com.example.tsecapp.Home.Model.ExpandableListItemModel;
import com.example.tsecapp.Home.adapters.ExpandableListAdapter;
import com.example.tsecapp.Home.fragments.CommitteeFragment;
import com.example.tsecapp.Home.fragments.ContactusFragment;
import com.example.tsecapp.Home.fragments.CourseFragment;
import com.example.tsecapp.Home.fragments.DepartmentFragment;
import com.example.tsecapp.Home.fragments.GalleryFragment;
import com.example.tsecapp.Home.fragments.PlacementFragment;
import com.example.tsecapp.R;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    Toolbar toolbar;
    DrawerLayout drawerLayout;
    ExpandableListView expandableListDrawer;
    ExpandableListAdapter expandableListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_hamburger);


        drawerLayout = findViewById(R.id.drawer);
        expandableListDrawer = findViewById(R.id.elv_drawer_menu);


        getSupportFragmentManager().beginTransaction().replace(R.id.fl_fragment_detail, new CourseFragment()).commit();

        List<ExpandableListItemModel> expandableList = setupData();
        expandableListAdapter = new ExpandableListAdapter(expandableList, this);
        expandableListDrawer.setAdapter(expandableListAdapter);

        setUpNavigation(expandableListDrawer);
    }

    private void setUpNavigation(final ExpandableListView expandableListDrawer) {


        expandableListDrawer.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                Class fragmentClass;
                FragmentManager fragmentManager = getSupportFragmentManager();

                switch (groupPosition){

                    //courses
                    case 0:
                        fragmentClass = CourseFragment.class;
                        commitFragmentChanges(fragmentClass, fragmentManager);
                        break;

                    //department
                    case 1:
                        toggleGroup(groupPosition);
                        break;

                    //tpo
                    case 2:
                        toggleGroup(groupPosition);
                        break;

                    //gallery
                    case 3:
                        fragmentClass = GalleryFragment.class;
                        commitFragmentChanges(fragmentClass, fragmentManager);
                        break;

                    //committee
                    case 4:
                        fragmentClass = CommitteeFragment.class;
                        commitFragmentChanges(fragmentClass, fragmentManager);
                        break;

                    //contact us
                    case 5:
                        fragmentClass = ContactusFragment.class;
                        commitFragmentChanges(fragmentClass, fragmentManager);
                        break;


                    case 6:
                        break;



                    default:
                        fragmentClass = CourseFragment.class;
                        commitFragmentChanges(fragmentClass, fragmentManager);
                        break;


                }


//                drawerLayout.closeDrawers();



                return true;
            }
        });
    }

    private void commitFragmentChanges(Class fragmentClass, FragmentManager fragmentManager) {
        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        fragmentManager.beginTransaction().replace(R.id.fl_fragment_detail, fragment).commit();

        drawerLayout.closeDrawers();
    }

    private void toggleGroup(int groupPosition) {
        if(expandableListDrawer.isGroupExpanded(groupPosition))
            expandableListDrawer.collapseGroup(groupPosition);
        else
            expandableListDrawer.expandGroup(groupPosition, true);
    }

    private List<ExpandableListItemModel> setupData() {

        List<ExpandableListItemModel> list = new ArrayList<>();

        List<String> departmentChildren = new ArrayList<>();
        List<String> tpoChildren = new ArrayList<>();
        List<String> noChildren = new ArrayList<>();

        list.add(new ExpandableListItemModel("Home", R.drawable.ic_home, noChildren));


        departmentChildren.add("Computer Engineering");
        departmentChildren.add("Information Technology");
        departmentChildren.add("Chemical Engineering");
        departmentChildren.add("Bio-Medical Engineering");
        departmentChildren.add("Bio-Tech Engineering");
        departmentChildren.add("Electronics & Telecommunications Engineering");
        list.add(new ExpandableListItemModel("Department", R.drawable.ic_department, departmentChildren));


        tpoChildren.add("Placement Statistics");
        tpoChildren.add("Our Recruiters");
        list.add(new ExpandableListItemModel("TPO", R.drawable.ic_tpo, tpoChildren));

        list.add(new ExpandableListItemModel("Gallery", R.drawable.ic_gallery, noChildren));
        list.add(new ExpandableListItemModel("Committee", R.drawable.ic_committee, noChildren));
        list.add(new ExpandableListItemModel("Contact Us", R.drawable.ic_contactus, noChildren));
        list.add(new ExpandableListItemModel("Sign Out", R.drawable.ic_signout, noChildren));

        return list;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                drawerLayout.openDrawer(Gravity.START);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
