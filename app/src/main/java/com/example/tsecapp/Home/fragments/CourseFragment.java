package com.example.tsecapp.Home.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.tsecapp.R;

public class CourseFragment extends Fragment {

    View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_courses, container, false);

        LinearLayout ll = view.findViewById(R.id.ll_course_parent);
        CardView courseCard = (CardView) inflater.inflate(R.layout.card_course_item, null, false);
        ll.addView(courseCard);
        ll.addView(courseCard);
        return view;
    }
}
